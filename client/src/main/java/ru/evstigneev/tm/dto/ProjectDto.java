package ru.evstigneev.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.lang.NonNullApi;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Date;

@JacksonXmlRootElement(localName = "project")
public class ProjectDto {

    @NotNull
    private String name;
    @Nullable
    private String id;
    @Nullable
    private String description;
    @Nullable
    private String dateOfCreation;
    @Nullable
    private String dateStart;
    @Nullable
    private String dateFinish;
    @Nullable
    private Status status;

    @Override
    public String toString() {
        return "Project ID:" + getId() + " | Project name: "
                + getName() + " | Project description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Project status: " + getStatus();
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

}
