package ru.evstigneev.tm.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.enumerated.Status;

@JacksonXmlRootElement(localName = "task")
public class TaskDto {

    @Nullable
    private String dateOfCreation;
    @NotNull
    private String name;
    @NotNull
    private String id;
    @NotNull
    private String projectId;
    @Nullable
    private String description;
    @Nullable
    private String dateStart;
    @Nullable
    private String dateFinish;
    @NotNull
    private Status status;

    @Override
    public String toString() {
        return "Project ID:" + getProjectId() + " | Task ID:" + getId() + " | Task name: "
                + getName() + " | Task description: " + getDescription() + " | Date of creation: "
                + getDateOfCreation() + " | Date of start: " + getDateStart() + " | Date of finish: "
                + getDateFinish() + " | Task status: " + getStatus();
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(@NotNull final String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(@Nullable final String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@Nullable final String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

}
