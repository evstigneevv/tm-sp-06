package ru.evstigneev.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.enumerated.Status;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Named(value = "taskBean")
@ViewScoped
public class TaskBean {

    @NotNull
    private String id;
    @NotNull
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private Status status;
    @NotNull
    private String projectId;

    public String create(@NotNull final String projectId, @NotNull final String name, @Nullable final String description) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setProjectId(projectId);
        taskDto.setName(name);
        taskDto.setDescription(description);
        restTemplate.postForEntity("http://localhost:8080/tasks", taskDto, TaskDto.class);
        return "/projectTaskList?id=" + projectId + "faces-redirect=true";
    }

    public String update(@NotNull final String id, @NotNull final String name,
                         @Nullable final String description,
                         @Nullable final String dateStart,
                         @Nullable final String dateFinish, @NotNull final Status status) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(id);
        taskDto.setName(name);
        taskDto.setDescription(description);
        taskDto.setDateStart(dateStart);
        taskDto.setDateFinish(dateFinish);
        taskDto.setStatus(status);
        restTemplate.put("http://localhost:8080/tasks", taskDto, TaskDto.class);
        return "/projectTaskList?id=" + projectId + "faces-redirect=true";
    }

    public List<TaskDto> findAllByProject(@NotNull final String projectId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<TaskDto[]> responseEntity = restTemplate.getForEntity("http://localhost:8080" +
                "/tasks/project?id=" + projectId, TaskDto[].class);
        return Arrays.asList(responseEntity.getBody());
    }

    public List<TaskDto> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<TaskDto[]> responseEntity = restTemplate.getForEntity("http://localhost:8080" +
                "/tasks", TaskDto[].class);
        return Arrays.asList(responseEntity.getBody());
    }

    public String delete(@NotNull final String projectId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete("http://localhost:8080/tasks?id=" + projectId);
        return "/projectTaskList?id=" + projectId + "faces-redirect=true";
    }

    public TaskDto findOne(@NotNull final String taskId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity("http://localhost:8080/task?id=" + taskId, TaskDto.class).getBody();
    }

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@NotNull final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@NotNull final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@NotNull final String projectId) {
        this.projectId = projectId;
    }

}
