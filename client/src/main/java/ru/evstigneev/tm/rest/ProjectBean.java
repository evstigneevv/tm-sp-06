package ru.evstigneev.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.enumerated.Status;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Named(value = "projectBean")
@ViewScoped
public class ProjectBean {

    @NotNull
    private String id;
    @NotNull
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private Status status;

    public String create(@NotNull final String name, @Nullable final String description) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName(name);
        projectDto.setDescription(description);
        restTemplate.postForObject("http://localhost:8080/projects", projectDto, ProjectDto.class);
        return "/projectList?faces-redirect=true";
    }

    public String update(@NotNull final String id, @NotNull final String name, @Nullable final String description,
                         @Nullable final String dateStart,
                         @Nullable final String dateFinish, @NotNull final Status status) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ProjectDto projectDto = findOne(id);
        projectDto.setName(name);
        projectDto.setDescription(description);
        projectDto.setDateStart(dateStart);
        projectDto.setDateFinish(dateFinish);
        projectDto.setStatus(status);
        restTemplate.put("http://localhost:8080/projects", projectDto, ProjectDto.class);
        return "/projectList?faces-redirect=true";
    }

    public List<ProjectDto> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDto[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/projects",
                ProjectDto[].class);
        return Arrays.asList(responseEntity.getBody());
    }

    public String delete(@NotNull final String projectId) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete("http://localhost:8080/projects?id=" + projectId);
        return "/projectList?faces-redirect=true";
    }

    public ProjectDto findOne(@NotNull final String projectId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity("http://localhost:8080/project?id=" + projectId, ProjectDto.class).getBody();
    }

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@NotNull final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@NotNull final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

}
