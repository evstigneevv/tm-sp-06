package ru.evstigneev.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.util.DateParser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class ProjectRestController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects")
    public List<ProjectDto> findAll() {
        return new ArrayList<>(createProjectDtoList(projectService.findAll()));
    }

    @GetMapping("/project")
    public ProjectDto findOne(@RequestParam(value = "id") @NotNull final String projectId) throws Exception {
        return convertProjectToDto(projectService.findOne(projectId));
    }

    @PostMapping("/projects")
    public HttpStatus create(@RequestBody @NotNull final ProjectDto project) throws ParseException {
        projectService.create(project);
        return HttpStatus.CREATED;
    }

    @DeleteMapping("/projects")
    public HttpStatus remove(@RequestParam(value = "id") @NotNull final String projectId) throws Exception {
        projectService.remove(projectId);
        return HttpStatus.OK;
    }

    @PutMapping("/projects")
    public HttpStatus update(@RequestBody @NotNull final ProjectDto projectDto) throws Exception {
        projectService.update(projectDto.getId(), projectDto.getName(), projectDto.getDescription(),
                DateParser.setDateByString(projectDto.getDateStart()),
                DateParser.setDateByString(projectDto.getDateFinish()),
                projectDto.getStatus().displayName());
        return HttpStatus.OK;
    }

    private ProjectDto convertProjectToDto(@NotNull final Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setDateOfCreation(DateParser.getDateString(project.getDateOfCreation()));
        projectDto.setDateStart(DateParser.getDateString(project.getDateStart()));
        projectDto.setDateFinish(DateParser.getDateString(project.getDateFinish()));
        projectDto.setStatus(project.getStatus());
        return projectDto;
    }

    private Collection<ProjectDto> createProjectDtoList(@NotNull final Collection<Project> projectList) {
        @NotNull final Collection<ProjectDto> projectDtoList = new ArrayList<>();
        for (Project project : projectList) {
            projectDtoList.add(convertProjectToDto(project));
        }
        return projectDtoList;
    }

}
