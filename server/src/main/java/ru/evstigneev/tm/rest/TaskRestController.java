package ru.evstigneev.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.evstigneev.tm.dto.TaskDto;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.util.DateParser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RequestMapping()
@RestController
public class TaskRestController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public List<TaskDto> findAll() {
        return new ArrayList<>(createTaskDtoList(taskService.findAll()));
    }

    @GetMapping("/task")
    public TaskDto findOne(@RequestParam(value = "id") @NotNull final String taskId) throws Exception {
        return convertTaskToDto(taskService.findOne(taskId));
    }

    @GetMapping("/tasks/project")
    public List<TaskDto> findAllByProject(@RequestParam(value = "id") @NotNull final String projectId) {
        return createTaskDtoList(taskService.findAllByProject(projectId));
    }

    @PostMapping("/tasks")
    public HttpStatus create(@RequestBody @NotNull final TaskDto task) throws Exception {
        taskService.create(task);
        return HttpStatus.CREATED;
    }

    @DeleteMapping("/tasks")
    public HttpStatus remove(@RequestParam(value = "id") @NotNull String taskId) throws Exception {
        taskService.remove(taskId);
        return HttpStatus.OK;
    }

    @PutMapping("/tasks")
    public HttpStatus update(@RequestBody @NotNull final TaskDto taskDto) throws Exception {
        taskService.update(taskDto.getId(), taskDto.getName(), taskDto.getDescription(), taskDto.getDateStart(),
                taskDto.getDateFinish(), taskDto.getStatus());
        return HttpStatus.OK;
    }

    private TaskDto convertTaskToDto(@NotNull final Task task) {
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setId(task.getId());
        taskDto.setProjectId(task.getProject().getId());
        taskDto.setName(task.getName());
        taskDto.setDescription(task.getDescription());
        taskDto.setDateOfCreation(DateParser.getDateString(task.getDateOfCreation()));
        taskDto.setDateStart(DateParser.getDateString(task.getDateStart()));
        taskDto.setDateFinish(DateParser.getDateString(task.getDateFinish()));
        taskDto.setStatus(task.getStatus());
        return taskDto;
    }

    private List<TaskDto> createTaskDtoList(@NotNull final Collection<Task> taskList) {
        @NotNull final List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(convertTaskToDto(task));
        }
        return taskDtoList;
    }

}