package ru.evstigneev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Named
@ViewScoped
public class DateParser {

    public static String getDateString(@Nullable final Date date) {
        if (date == null) return "Is not set yet";
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(date);
    }

    public static Date setDateByString(@Nullable final String dateString) {
        if (dateString == null || dateString.isEmpty() || dateString.equals("Is not set yet")) return null;
        List<String> formatStrings = Arrays.asList("dd.MM.yyyy", "yyyy-MM-dd HH:mm:ss", "EEE MMM dd HH:mm:ss z yyyy");
        for (String formatString : formatStrings) {
            try {
                return new SimpleDateFormat(formatString, Locale.ENGLISH).parse(dateString);
            } catch (ParseException ignored) {
            }
        }
        throw new IllegalArgumentException();
    }

}
