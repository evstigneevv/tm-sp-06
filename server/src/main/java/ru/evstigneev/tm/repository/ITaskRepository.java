package ru.evstigneev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Collection;
import java.util.Date;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    @Modifying
    @Query("UPDATE Task task SET task.name = :name, task.description = :description, task.dateStart = :dateStart, task.dateFinish = " +
            ":dateFinish, task.status = :status WHERE task.id = :id")
    void update(@Param("id") final String id, @Param("name") final String name, @Param("description") final String description,
                @Param("dateStart") final Date dateStart, @Param("dateFinish") final Date dateFinish,
                @Param("status") final Status status);

    @Query("select task from Task task where task.project.id = :projectId")
    Collection<Task> findAllByProject(@Param("projectId") final String projectId);

}
