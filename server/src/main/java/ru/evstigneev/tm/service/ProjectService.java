package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evstigneev.tm.dto.ProjectDto;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.repository.IProjectRepository;
import ru.evstigneev.tm.util.DateParser;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Transactional
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Transactional
    public void create(@NotNull final ProjectDto project) throws ParseException {
        project.setId(UUID.randomUUID().toString());
        project.setDateOfCreation(DateParser.getDateString(new Date()));
        project.setStatus(Status.PLANNING);
        projectRepository.save(convertDtoToProject(project));
    }

    @Transactional
    public void persist(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Transactional
    public void remove(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        projectRepository.delete(findOne(projectId));
    }

    @Transactional
    public void update(@NotNull final String projectId,
                       @NotNull final String name, @Nullable final String description,
                       @Nullable final Date dateStart, @Nullable final Date dateFinish,
                       @NotNull final String status) throws Exception {
        if (projectId.isEmpty() || name.isEmpty() || description == null || description.isEmpty() || status.isEmpty()) {
            throw new IllegalArgumentException();
        }
        projectRepository.update(projectId, name, description, dateStart,
                dateFinish, Status.valueOf(status.toUpperCase()));
    }

    @Transactional
    public Project findOne(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return projectRepository.findById(projectId).orElse(null);
    }

    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    private Project convertDtoToProject(@NotNull final ProjectDto projectDto) throws ParseException {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setDateOfCreation(DateParser.setDateByString(projectDto.getDateOfCreation()));
        project.setDateStart(DateParser.setDateByString(projectDto.getDateStart()));
        project.setDateFinish(DateParser.setDateByString(projectDto.getDateFinish()));
        project.setStatus(projectDto.getStatus());
        return project;
    }

}
